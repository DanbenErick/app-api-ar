import { AuditoriaMap } from '../Auditoria';

export interface TipoTransaccionClase {
  ID_TIPO_TRAN?: string;
  DE_NOMB_TRAN?: string;
  CO_TIPO?: string;
  ID_UNID_OPER?: string;
  CO_NAME?: string;
}

export interface TipoTransaccionBr {
  ID_TIPO_TRX_VTA?: number;
  CO_NAME?: string;
  ID_UNID_OPER?: number;
}

export interface DocumentoCobrarCabeceraVista extends AuditoriaMap {
  ID_UNID_OPER?: number;
  DE_NOMB_ORGN?: string;
  DE_NOMB_CLTE?: string;
  DE_NOMB_TRAN?: string;
  DE_TIPO_DOCU?: string;
  NU_TRX?: string;
  FE_TRX?: Date;
  FE_TRX_GL?: Date;
  CO_MNDA?: string;
  NU_MNTO_DOCU_BR?: number;
  ID_ORGN?: number;
  DE_ETPA?: string;
  DE_ESTA?: string;
  ID_ETPA?: number;
  ID_ESTA?: number;
  ID_DOCU_VNTA?: number;
  ID_CNTA_CLTE?: number;
  NU_TASA_CONV?: number;
  NU_TASA_CONV_COBR?: number;
  ID_TIPO_TRAN?: number;
  ID_TIPO_TRX_CUST?: number;
  CO_TIPO_TRAN?: string;
  FE_VENC?: Date;
  ID_GIRO?: number;
  ID_DIRE_GIRO?: number;
  IN_FIRM?: string;
}
