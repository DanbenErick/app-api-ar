export interface Auditoria {
  co_usua_crea?: string;
  fe_crea?: Date;
  ip_term_crea?: string;
  co_usua_modi?: string;
  fe_modi?: Date;
  ip_term_modi?: string;
}

export interface AuditoriaMap {
  CO_USUA_CREA?: string;
  FE_CREA?: Date;
  IP_TERM_CREA?: string;
  CO_USUA_MODI?: string;
  FE_MODI?: Date;
  IP_TERM_MODI?: string;
}

export class AuditoriaCreacion {
  usuario: string;
  fechaCrea: Date;
  ipTerm: string;

  constructor() {
    this.usuario = '';
    this.fechaCrea = new Date();
    this.ipTerm = '';
  }
}

export class AuditoriaModificacion {
  usuario: string;
  fechaModi: Date;
  ipTerm: string;

  constructor() {
    this.usuario = '';
    this.fechaModi = new Date();
    this.ipTerm = '';
  }
}
