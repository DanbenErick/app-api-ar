import { Request } from 'express';
import url from 'url';
import querystring from 'querystring';
import path from 'path';
//var format = require('date-format');

const periodos = [
  'ENE',
  'FEB',
  'MAR',
  'ABR',
  'MAY',
  'JUN',
  'JUL',
  'AGO',
  'SEP',
  'OCT',
  'NOV',
  'DIC',
];

//export default class Util {
//constructor() { }

export function fromStringToDate(fecha: string) {
  if (fecha && fecha != null && fecha.trim() != '') {
    const mes: any = fecha.split('-')[0];
    if (periodos.indexOf(mes) != -1) {
      let indice = 0;
      let numberMes = 0;
      periodos.forEach((periodo) => {
        if (periodo == mes) {
          numberMes = indice;
        }
        indice++;
      });
      const anio = Number.parseInt('20'.concat(fecha.split('-')[1]));
      return new Date(anio, numberMes, 1, 0, 0, 0);
    } else {
      return null;
    }
  }
}

export function numberWithCommas(x: number) {
  var parts = x.toFixed(2).toString().split('.');
  console.log('parts', parts);
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  //if (!parts[1]){ parts[1] = '00'}else{ parts[1] = lpad(Number(parts[1]),2)}
  if (!parts[1]) {
    parts[1] = '00';
  }
  return parts.join('.');
}

export function FromDateToString(fecha: Date): any {
  let fechaString = null;
  if (fecha) {
    const date = fecha;
    let dia = date.getDate();
    let diaString = '00';
    if (dia < 10) diaString = '0' + dia;
    else diaString = dia.toString();
    const mes = date.getMonth() + 1;
    let mesString = '00';
    if (mes < 10) mesString = '0' + mes;
    else mesString = mes.toString();
    const anio = date.getFullYear().toString();
    fechaString = diaString
      .concat('/')
      .concat(mesString)
      .concat('/')
      .concat(anio);
  }
  return fechaString;
}

export function obtenerFechaActual() {
  const date = new Date();

  const fecha = FromDateToString(date);
  const fechaHora =
    fecha.concat(' ') +
    date.getHours() +
    ':' +
    date.getMinutes() +
    ':' +
    date.getSeconds();
  return fechaHora;
}

export function FromDateAnioMesDiaToString(fecha: Date): any {
  let fechaString = null;
  if (fecha) {
    const date = fecha;
    let dia = date.getDate();
    let diaString = '00';
    if (dia < 10) diaString = '0' + dia;
    else diaString = dia.toString();
    const mes = date.getMonth() + 1;
    let mesString = '00';
    if (mes < 10) mesString = '0' + mes;
    else mesString = mes.toString();
    const anio = date.getFullYear().toString();
    fechaString = anio
      .concat('/')
      .concat(mesString)
      .concat('/')
      .concat(diaString);
  }
  return fechaString;
}

export function redondearNumero(value: any, decimals: any) {
  value = value || 0;
  return Number(
    Math.round(Number(value + 'e' + decimals)) + 'e-' + decimals
  ).toFixed(decimals);
}

export function fixedNumber(value: number, decimals: number) {
  let resultado: any;

  switch (decimals) {
    case 1:
      resultado = Math.round(value * 10) / 10;
      break;
    case 2:
      resultado = Math.round(value * 100) / 100;
      break;
    case 3:
      resultado = Math.round(value * 1000) / 1000;
      break;
    case 4:
      resultado = Math.round(value * 10000) / 10000;
      break;
    case 5:
      resultado = Math.round(value * 100000) / 100000;
      break;
  }

  return resultado;
}

// export function setValuesAuditoria(tipo: string, object: any, auditoria: AuditoriaCreacion){

//     if (tipo == 'I'){
//         object.CO_USUA_CREA = auditoria.usuario;
//         object.FE_CREA = auditoria.fechaCrea;
//         object.IP_TERM_CREA = auditoria.ipTerm;
//     }else if (tipo == 'U'){
//         object.CO_USUA_MODI = auditoria.usuario;
//         object.FE_MODI = auditoria.fechaCrea;
//         object.IP_TERM_MODI = auditoria.ipTerm;
//     }
//     return object;
// }

// export function transformDateFormat(date: Date, formato: string) {
//     const mes = date.getMonth();
//     let mesTransform: string = '';
//     switch (mes) {
//         case 0:
//             mesTransform = 'ENE';
//             break;
//         case 1:
//             mesTransform = 'FEB';
//             break;
//         case 2:
//             mesTransform = 'MAR';
//             break;
//         case 3:
//             mesTransform = 'ABR';
//             break;
//         case 4:
//             mesTransform = 'MAY';
//             break;
//         case 5:
//             mesTransform = 'JUN';
//             break;
//         case 6:
//             mesTransform = 'JUL';
//             break;
//         case 7:
//             mesTransform = 'AGO';
//             break;
//         case 8:
//             mesTransform = 'SEP';
//             break;
//         case 9:
//             mesTransform = 'OCT';
//             break;
//         case 10:
//             mesTransform = 'NOV';
//             break;
//         case 11:
//             mesTransform = 'DIC';
//             break;
//     }

//     if (mesTransform) {
//         const dpTransf = format(formato, date);
//         return mesTransform.concat("-" + dpTransf);
//     } else return format(date, formato);

// }

export var zeroFill = async (number: any, width: number) => {
  width -= number.toString().length;
  if (width > 0) {
    return new Array(width + (/\./.test(number) ? 2 : 1)).join('0') + number;
  }
  return number + ''; // siempre devuelve tipo cadena
};

export var reflectObject = async (objectNew: any, objectCurrent: any) => {
  for (let key of Object.keys(objectCurrent)) {
    if (key.substring(0, 2) == 'FE')
      objectNew[key] =
        (objectCurrent[key] && new Date(objectCurrent[key])) || null;
    else objectNew[key] = (objectCurrent[key] && objectCurrent[key]) || null;
  }
  return objectNew;
};

export var updateObjectNewToCurrent = async (
  objectNew: any,
  objectCurrent: any
) => {
  for (let key of Object.keys(objectNew)) {
    if (objectCurrent[key]) {
      if (key.substring(0, 2) == 'FE')
        objectCurrent[key] =
          (objectNew[key] && new Date(objectNew[key])) || null;
      else objectCurrent[key] = (objectNew[key] && objectNew[key]) || null;
    }
  }
  return objectCurrent;
};

export var createNewObjectFromCurrent = async (
  objectCurrent: any,
  objectNew: any
) => {
  for (let key of Object.keys(objectCurrent)) {
    if (key.substring(0, 2) == 'FE')
      objectNew[key] =
        (objectCurrent[key] && new Date(objectCurrent[key])) || null;
    else objectNew[key] = (objectCurrent[key] && objectCurrent[key]) || null;
  }
  return objectNew;
};

export var createObjectWithMetaData = async (metaData: any) => {
  let objectNew: any = {};

  for (let campo of metaData) {
    const key = campo.name;
    objectNew[key] = null;
  }
  return objectNew;
};

export var obtenerMetaDataTabla = async (connection: any, params: any) => {
  let query: string;
  let bind: any = {};

  query = ` SELECT * FROM ${params.tabla} WHERE 1=2 `;
  const result: any = await connection.execute(query, bind);
  return result.metaData;
};

export var getValuesObjectNewFromCurrent = async (
  objectMetaData: any,
  objectCurrent: any
) => {
  let objectNew: any = {};
  for (let key of Object.keys(objectMetaData)) {
    if (!(objectCurrent[key] == null)) {
      //si es nulo o indefinido
      if (key.substring(0, 2) == 'FE')
        objectNew[key] =
          (objectCurrent[key] && new Date(objectCurrent[key])) || null;
      else objectNew[key] = objectCurrent[key];
    }
  }
  return objectNew;
};

export var createNewObjectFromMetaData = async (
  metaData: any,
  objectCurrent: any,
  objectNew: any
) => {
  for (let campo of metaData) {
    //console.log(campo.name);
    const key = campo.name;
    if (!(objectCurrent[key] == null)) {
      //si es nulo o indefinido
      if (key.substring(0, 2) == 'FE')
        objectNew[key] =
          (objectCurrent[key] && new Date(objectCurrent[key])) || null;
      else objectNew[key] = objectCurrent[key];
    } else {
      console.log(objectCurrent[key]);
    }
  }
  return objectNew;
};

export var removeNullFields = async (objectCurrent: any) => {
  let objectNew: any = {};

  for (let key of Object.keys(objectCurrent)) {
    if (objectCurrent[key] != null) objectNew[key] = objectCurrent[key];
  }
  return objectNew;
};

export var obtenerParametrosRequest = async (req: Request) => {
  let rawUrl = req.url;
  let parsedUrl: any = url.parse(rawUrl);
  let parsedQs = querystring.parse(parsedUrl.query);
  return JSON.parse(JSON.stringify(parsedQs));
};

export function lpad(num: number, size: number) {
  let s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}

export function obtenerFonts() {
  const ruta = __dirname.replace('/dist/util', '');
  const fonts = {
    Roboto: {
      normal: path.resolve(`${ruta}/assets/fonts/Roboto-Regular.ttf`),
      bold: path.resolve(`${ruta}/assets/fonts/Roboto-Medium.ttf`),
      italics: path.resolve(`${ruta}/assets/fonts/Roboto-Italic.ttf`),
      bolditalics: path.resolve(`${ruta}/assets/fonts/Roboto-MediumItalic.ttf`),
    },
  };
  //console.log('dirname', __dirname);
  //console.log('ruta', ruta);
  return fonts;
}

export function obtenerPathAbsolute() {
  return __dirname.replace('/dist/util', '');
}

export var obtenerQuery = async (
  accion: string,
  tabla: string,
  campos: any,
  where: any[]
) => {
  let resultado: string = '';
  //
  let queryTabla: string = tabla;
  let queryCampos: string = '';
  let queryParametros: string = '';
  let queryWhere: string = ' WHERE 1=1 ';
  //
  switch (accion) {
    case 'UPDATE':
      queryTabla = 'UPDATE ' + tabla + ' SET ';
      queryCampos = '';
      queryWhere = ' WHERE 1=1 ';

      for (let key of Object.keys(campos)) {
        const existe = where.filter(
          (f) => f.ID.toUpperCase() == key.toUpperCase()
        ).length;
        if (existe == 0)
          queryCampos += key.toLowerCase() + '=' + ':' + key + ',';
      }
      queryCampos = queryCampos.substring(0, queryCampos.length - 1);

      for (let t of where) {
        queryWhere += ' AND ' + t.ID + ' = ' + ':' + t.ID;
      }

      resultado = queryTabla + queryCampos + queryWhere;
      break;
    case 'INSERT':
      queryTabla = 'INSERT INTO ' + tabla + ' ';
      queryCampos = '';

      queryWhere = ' WHERE 1=1 ';

      for (let key of Object.keys(campos)) {
        queryCampos += key.toLowerCase() + ',';
        queryParametros += ':' + key + ',';
      }

      queryCampos = queryCampos.substring(0, queryCampos.length - 1);
      queryParametros = queryParametros.substring(
        0,
        queryParametros.length - 1
      );

      resultado =
        queryTabla + '(' + queryCampos + ') VALUES (' + queryParametros + ')';
      break;
  }
  return resultado;
};

//}
