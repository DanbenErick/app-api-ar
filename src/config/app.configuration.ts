/* 
  Modulos Externos
*/
import express from 'express';
import path from 'path';
import cors from 'cors';

/* 
  Rutas Propias
*/
import { ApiRoutes } from '../app-routing.module';
class AppConfiguracion {
  public app: express.Application;
  private ApiRoutes = new ApiRoutes();

  public constructor() {
    this.app = express();
    this.initApp();
  }

  public initApp(): void {
    this.app.use(express.static(path.join(__dirname, './../public')));
    this.app.use(
      cors({
        origin: '*',
        exposedHeaders: 'Content-Disposition',
      })
    );
    this.app.use(this.ApiRoutes.app);
  }
}

export default AppConfiguracion;
