import oracledb from 'oracledb';
import {
  ConnectionProdOracle,
  ConnectionDesaOracle,
} from '../resources/properties.resource';

var pool: any;

process.env.UV_THREADPOOL_SIZE = '24';
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

class connectionOracle {
  constructor() {}

  public conectarOracle = async () => {
    /* Desarrollo */
    return new Promise(async (resolve) => {
      resolve(
        oracledb.getConnection({
          user: ConnectionDesaOracle.user,
          password: ConnectionDesaOracle.pass,
          connectString: ConnectionDesaOracle.cadena,
        })
      );
    });

    /* Producción */

    // return new Promise(async resolve => {
    //     resolve(oracledb.getConnection({
    //         user: ConnectionProdOracle.user,
    //         password: ConnectionProdOracle.pass,
    //         connectString: ConnectionProdOracle.cadena
    //     }));
    // });
  };

  connectionClose = async () => {
    return new Promise((resolve) => {
      resolve(pool.close());
    });
  };
}

export default new connectionOracle();
