import {
  DocumentoCobrarCabeceraVista,
  TipoTransaccionBr,
  TipoTransaccionClase,
} from '../../dto/documento-cobrar/documento-cobrar.dto';
import { logger } from '../../resources/manager-log.resource';

export class DocumentosCobrarRepository {
  public obtenerOrigen = async (connection: any, params: any) => {
    try {
      let bind: any = {};
      let query = `
            SELECT * FROM ds_vis_mst_ar_tipo_tran_clse tv
            WHERE tv.co_tipo = 'BR'
            AND tv.id_unid_oper = :id_unid_oper
            `;

      bind.id_unid_oper = params.idUnidOper;

      console.log('query', query, bind);
      const result: any = await connection.execute(query, bind);
      return result.rows as TipoTransaccionClase[];
    } catch (err) {
      //logger.error(err);
      throw err;
    }
  };

  public obtenerTipo = async (connection: any, params: any) => {
    try {
      let bind: any = {};
      let query = `
            SELECT tv.id_tipo_trx_vta, tv.co_name, tv.id_unid_oper
            FROM ds_tab_mst_tipo_trx_vnta tv
            WHERE tv.id_unid_oper = :id_unid_oper
            `;

      bind.id_unid_oper = params.idUnidOper;

      console.log('query', query, bind);
      const result: any = await connection.execute(query, bind);
      return result.rows as TipoTransaccionBr[];
    } catch (err) {
      logger.error(err);
      throw err;
    }
  };

  public obtenerDocumentoCobrarCabecera = async (
    connection: any,
    params: any
  ) => {
    let bind: any;
    let query: string = '';
    try {
      bind = {};
      if (params.nuRegi) {
        query += `SELECT *
                            FROM (
                `;
      }
      query += `
                    SELECT *
                      FROM ds_vis_trx_ar_docu_cobr_cabe c
                    WHERE 1=1
                      AND c.id_unid_oper = :id_unid_oper
            `;

      if (params.nuRegi) {
        query += ` ORDER BY c.fe_trx DESC )
                            AND ROWNUM <= :nu_regi
                `;
        bind.nu_regi = params.nuRegi;
      }

      bind.id_unid_oper = params.idUnidOper;

      if (params.idCntaClte) {
        query += ` AND c.id_cnta_clte = :id_cnta_clte `;
        bind.id_cnta_clte = params.idCntaClte;
      }

      if (params.feTrxInic) {
        query += ` AND trunc(c.fe_trx) BETWEEN trunc(to_date(:fe_trx_inic,'dd/mm/rrrr')) AND nvl(trunc(to_date(:fe_trx_fina,'dd/mm/rrrr')), SYSDATE)`;
        bind.nu_trx = `%${params.nuTrx}%`;
      }

      const result: any = await connection.execute(query, bind);
      return result.rows as DocumentoCobrarCabeceraVista[];
    } catch (err) {
      logger.error('obtenerDocumentoCobrarCabecera', String(err), query, bind);
      throw err;
    }
  };
}
