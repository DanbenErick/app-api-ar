/* 
  Configuracion del Servidor 
*/
require('dotenv').config();
import ServerConfig from './config/server';

class Index {
  public constructor() {
    new ServerConfig();
  }
}

new Index();
