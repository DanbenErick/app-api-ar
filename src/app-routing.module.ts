// Modulos Externos
import cors from 'cors';
import express from 'express';

import DocumentoCobrarRouting from './controllers/documentos-cobrar.routing';

class ApiRoutes {
  public app = express();

  public constructor() {
    this.routes();
  }

  public routes() {
    this.app.use('/', DocumentoCobrarRouting);
  }
}

export { ApiRoutes };
