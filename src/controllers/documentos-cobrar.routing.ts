import { Router } from 'express';
import { DocumentosCobrarController } from './documentos-cobrar/documentos-cobrar.rest.api';

class DocumentosCobrarRouting {
  public router;
  public documentosCobrarRest: DocumentosCobrarController;

  public constructor() {
    this.documentosCobrarRest = new DocumentosCobrarController();
    this.router = Router();
    this.routes();
  }

  public routes() {
    this.router.use('/documentos-cobrar', this.documentosCobrarRest.router);
  }
}

export default new DocumentosCobrarRouting().router;
