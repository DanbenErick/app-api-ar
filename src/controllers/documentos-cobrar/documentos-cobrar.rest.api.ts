import { Router, Request, Response } from 'express';
import expressAsyncHandler from 'express-async-handler';
import { DocumentosCobrarService } from '../../services/documento-cobrar.service';
import { logger } from '../../resources/manager-log.resource';
import { ServerError } from '../../resources/mensajes.constant';
import { obtenerParametrosRequest } from '../../util/util';

class DocumentosCobrarController {
  public router: Router;

  documentosCobrarService: DocumentosCobrarService;

  public constructor() {
    this.documentosCobrarService = new DocumentosCobrarService();
    //
    this.router = Router();
    this.routes();
  }

  obtenerOrigen = async (req: Request, res: Response) => {
    try {
      //console.log('req', req);
      const { idUnidOper } = await obtenerParametrosRequest(req);
      const params = { idUnidOper: idUnidOper };
      const result = await this.documentosCobrarService.obtenerOrigen(params);
      res.status(200).json(result);
    } catch (error) {
      logger.error(String(error));
      res.status(500).json(ServerError);
    }
  };

  obtenerTipo = async (req: Request, res: Response) => {
    try {
      const { idUnidOper } = await obtenerParametrosRequest(req);
      const params = { idUnidOper: idUnidOper };
      const result = await this.documentosCobrarService.obtenerTipo(params);
      res.status(200).json(result);
    } catch (error) {
      logger.error(String(error));
      res.status(500).json(ServerError);
    }
  };

  obtenerDocumentoCobrarCabecera = async (req: Request, res: Response) => {
    try {
      const { idUnidOper, idDocuVnta, feTrxInic, feTrxFina, idCntaClte } =
        await obtenerParametrosRequest(req);
      const params = {
        idUnidOper: idUnidOper,
        idDocuVenta: idDocuVnta,
        feTrxInic: feTrxInic,
        feTrxFina: feTrxFina,
        idCntaClte: idCntaClte,
      };
      const result =
        await this.documentosCobrarService.obtenerDocumentoCobrarCabecera(
          params
        );
      res.status(200).json(result);
    } catch (error) {
      logger.error(String(error));
      res.status(500).json(ServerError);
    }
  };

  routes() {
    this.router.get('/obtener-origen', expressAsyncHandler(this.obtenerOrigen));
    this.router.get('/obtener-tipo', expressAsyncHandler(this.obtenerTipo));
    this.router.get(
      '/obtener-documento-cobrar-listado',
      expressAsyncHandler(this.obtenerDocumentoCobrarCabecera)
    );
    //this.router.post('', asyncHandler(this.actualizarPeriodos));
    // this.router.put('', asyncHandler(this.actualizarPrecioCompra));
  }
}

export { DocumentosCobrarController };
