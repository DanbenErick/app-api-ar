import { TipoTransaccionClase } from '../dto/documento-cobrar/documento-cobrar.dto';
import { DocumentosCobrarRepository } from '../repositories/documentos-cobrar/documento-cobrar.repository';
import connectOracle from '../config/connection.oracledb';
import { logger } from '../resources/manager-log.resource';

export class DocumentosCobrarService {
  documentosCobrarRepository: DocumentosCobrarRepository;

  constructor() {
    this.documentosCobrarRepository = new DocumentosCobrarRepository();
  }

  public obtenerOrigen = async (params: any) => {
    let connection: any = await connectOracle.conectarOracle();
    try {
      const resultado: TipoTransaccionClase[] =
        await this.documentosCobrarRepository.obtenerOrigen(connection, params);
      return resultado;
    } catch (error) {
      logger.error('obtenerOrigen-serv:' + String(error));
      throw error;
    } finally {
      await connection.close();
    }
  };

  public obtenerTipo = async (params: any) => {
    let connection: any = await connectOracle.conectarOracle();
    try {
      const resultado = await this.documentosCobrarRepository.obtenerTipo(
        connection,
        params
      );
      return resultado;
    } catch (error) {
      logger.error('obtenerTipo-serv:' + String(error));
      throw error;
    } finally {
      await connection.close();
    }
  };

  public obtenerDocumentoCobrarCabecera = async (params: any) => {
    let connection: any = await connectOracle.conectarOracle();
    try {
      const resultado =
        await this.documentosCobrarRepository.obtenerDocumentoCobrarCabecera(
          connection,
          params
        );
      return resultado;
    } catch (error) {
      logger.error('obtenerDocumentoCobrarCabecera:' + String(error));
      throw error;
    } finally {
      await connection.close();
    }
  };
}
